console.log("Hello World");

//comment - ctrl /

//Arithmetic Operators
	// allow us to perform mathematical operations between operands (value on either sides of the operator).
	// it returns a numerical value.

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition operator: " + sum);//9228

//mini activity (4min)
//subtraction operator
//multiplication operator
//division operator
//modulo operator

let difference = x - y;
console.log("Result of subraction operator: " + difference);//-6434
let product = x * y;
console.log("Result of multiplication operator: " + product);//10939907
let quotient = x / y;
console.log("Result of division operator: " + quotient);//0.17839356404035245
let remainder = y % x;
console.log("Result of modulo operator: " + remainder);//846

// Basic Assignment Operator (=)
// The assignment operator assigns the value of the **right** operand to a variable.
let assignmentNumber = 8;

// Addition Assignment Operator (+=)
// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable. 

assignmentNumber = assignmentNumber + 2;//10
console.log("Result of addition assignment operator: " + assignmentNumber);//10

// Shorthand for assignmentNumber = assignmentNumber + 2 
assignmentNumber += 2;//10+=2 //12
console.log("Result of addition assignment operator: " + assignmentNumber);//12

//Subtraction/Multiplication/Division Assignment Operator
assignmentNumber -= 2;//12-=2 //10
console.log("Result of subraction assignment operator: " + assignmentNumber);//10

//Mini Activity
//Multiplication/Division Assignment Operator

assignmentNumber *= 2;//20
console.log("Result of multiplication assignment operator: " + assignmentNumber);
assignmentNumber /= 2;//10
console.log("Result of division assignment operator: " + assignmentNumber);


// Multiple Operators and Parentheses
//MDAS
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);//0.6
//3*4 = 12
//12/5 = 2.4
//1+2 = 3
//3 - 2.4 = 0.6

//PEMDAS
let pemdas = 1 + (2 - 3) * (4 / 5);//0.2
console.log("Result of pemdas oepration: " + pemdas);//

//4/5 = 0.8
//2-3 = -1
//-1 * 0.8 = -0.8
//1 + -0.8 = .2


//Increment and Decrement
//Operators that add or subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;
//pre-increment
//add and then reassign
//The value of "z" is added by a value of one before returning the value and storing it int he vartiable "increment"
let increment = ++z;
console.log("Result of pre-increment: " + increment);//2
//The value of "z" was also increased even though we didn't implicitly specify any value reassignment
console.log("Result of pre-increment: " + z);//2

//2
//post-increment
//reassign and then add
//The value of z is returned and stored in the variable increment then the value of z is increased by one
increment = z++;
//The value of z is at 2 before it was incremented
console.log("Result of post-increment: " + increment);//2
//the value of z was increased again, reassigning the value to 3
console.log("Result of post-increment: " + z);//3


// let a = 2;
// let increment1 = a++;
// console.log(increment1);

//subtract and then reassign
let decrement = --z;
console.log("Result of pre-decrement: " + decrement);//2
console.log("Result of pre-decrement: " + z);//2

// reassign and subtract
decrement = z--;
console.log("Result of post-decrement: " + decrement);//2
console.log("Result of post-decrement: " + z);//1

//Type Coercion

let numA = '10';//string
let numB = 12; //number

let coercion = numA + numB;
console.log(coercion);//1012
console.log(typeof coercion);//string

let numC = 16;//number
let numD = 14;//number

let nonCoercion = numC + numD;//30
console.log(nonCoercion);//30
console.log(typeof nonCoercion);//number

let numE = true + 1;
console.log(numE);//2 (1+1)

let numF = false + 1;
console.log(numF);//1 (0+1)


//Comparison Operators

let juan = 'juan';//assignment operator

//Equality Operator (==)

console.log(1==1);//true
console.log(1==2);//false
console.log(1=='1');//true
console.log(0==false);//true
console.log('juan' == 'juan');//true
console.log("juan" == juan);//true

//Strict Equality Operator

console.log(1===1);//true
console.log(1===2);//false
console.log(1==='1');//false
console.log(0===false);//false
console.log('juan' === 'juan');//true
console.log("juan" === juan);//true

//Inequality operator
console.log(1!=1);//false
console.log(1!=2);//true
console.log(1!='1');//false
console.log(0!=false);//false
console.log('juan' != 'juan');//false
console.log("juan" != juan);//false

//Strict Inequality operator
console.log(1!==1);//false
console.log(1!==2);//true
console.log(1!=='1');//true
console.log(0!==false);//true
console.log('juan' !== 'juan');//false
console.log("juan" !== juan);//false

//Relational Operators

let a = 50;
let b = 65;

let isGreaterThan = a>b;
let isLessThan = a<b;
let isGTorEqual = a >= b;
let isLTorEqual = a <= b;

console.log(isGreaterThan);//false
console.log(isLessThan);//true
console.log(isGTorEqual);//false
console.log(isLTorEqual);//true

//forced coercion to change the string into a number
let numStr = "30";
console.log(a>numStr);//50>30 //true
console.log(b<=numStr);//65<=30 //false
//70<30//false

console.log(30<=30);//true

//Logical Operators

let isLegalAge = true;
let isRegistered = false;
let isMarried = true;

//Logical And Operator (&&)

let allRequirementsMet = isLegalAge && isRegistered; //true && false
let example = isLegalAge && isMarried;//true && true //true
console.log("Result of logical AND Operator" + allRequirementsMet);//false

//Logical Or Operator (||)
let someRequirementsMet = isLegalAge || isRegistered; //true || false
console.log("Result of logical OR Operator: " + someRequirementsMet);//true

//Logical Not Operator (!)
let someRequirementsNotMet = !isRegistered; //!false //true

//Sample

let num5 = 23000;
let sampleRemainder = num5%5;
console.log("The remainder of " + num5 + " divided by 5 is: " + sampleRemainder);
let isDivisibleBy5 = sampleRemainder === 0;
console.log("Is num5 divisible by 5?");
console.log(isDivisibleBy5);//true
